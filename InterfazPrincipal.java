package interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.Color;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InterfazPrincipal extends JFrame {
	

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazPrincipal frame = new InterfazPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfazPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 747, 406);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("");
		label.setBounds(5, 5, 719, 0);
		contentPane.add(label);
		
		JLabel lblCocosAlmacenNatural = new JLabel("Coco's Almacen Natural");
		lblCocosAlmacenNatural.setForeground(Color.MAGENTA);
		lblCocosAlmacenNatural.setFont(new Font("Tahoma", Font.PLAIN, 37));
		lblCocosAlmacenNatural.setBounds(157, 72, 420, 52);
		contentPane.add(lblCocosAlmacenNatural);
		
		JLabel lblDieteticayherboristeria = new JLabel("Diet\u00E9tica y Herborister\u00EDa");
		lblDieteticayherboristeria.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDieteticayherboristeria.setBounds(157, 116, 210, 30);
		contentPane.add(lblDieteticayherboristeria);
		
		JButton btnClientes = new JButton("Clientes");
		btnClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Cliente verCli=new Cliente();
					verCli.setVisible(true);
			}
		});
		btnClientes.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnClientes.setBounds(50, 230, 144, 43);
		contentPane.add(btnClientes);
		
		JButton btnProveedores = new JButton("Proveedores");
		btnProveedores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Proveedor verProv=new Proveedor();
					verProv.setVisible(true);
				
			}
		});
		btnProveedores.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnProveedores.setBounds(282, 230, 144, 43);
		contentPane.add(btnProveedores);
		
		JButton btnArticulos = new JButton("Art\u00EDculos");
		btnArticulos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Articulos verArt=new Articulos();
				verArt.setVisible(true);
			}
		});
		btnArticulos.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnArticulos.setBounds(514, 230, 144, 43);
		contentPane.add(btnArticulos);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
					
			}
		});
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnSalir.setBounds(5, 5, 144, 43);
		contentPane.add(btnSalir);
	}
}
