package interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AgregarProveedor extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombreProveedor;
	private JTextField txtApellidoProveedor;
	private JTextField txtDireccionProveedor;
	private JTextField txtTelefonoProveedor;
	private JLabel lblEmpresaProveedor;
	private JTextField txtEmpresaProveedor;
	private JButton btnAgregarNuevoProveedor;
	private JButton btnCancelarNuevoProveedor;
	private int idProveedor = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AgregarProveedor frame = new AgregarProveedor(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @param idProveedor
	 */
	public AgregarProveedor(String selectedValue) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 552, 432);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Agregar Proveedor",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 0, 255)));
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(6, 2, 5, 5));

		JLabel lblNombreProveedor = new JLabel("Nombre");
		panel.add(lblNombreProveedor);

		txtNombreProveedor = new JTextField();
		txtNombreProveedor.setColumns(10);
		panel.add(txtNombreProveedor);

		JLabel lblApellidoProveedor = new JLabel("Apellido");
		panel.add(lblApellidoProveedor);

		txtApellidoProveedor = new JTextField();
		txtApellidoProveedor.setColumns(10);
		panel.add(txtApellidoProveedor);

		JLabel lblDireccionProveedor = new JLabel("Direcci\u00F3n");
		panel.add(lblDireccionProveedor);

		txtDireccionProveedor = new JTextField();
		txtDireccionProveedor.setColumns(10);
		panel.add(txtDireccionProveedor);

		JLabel lblTelefonoProveedor = new JLabel("Tel\u00E9fono");
		panel.add(lblTelefonoProveedor);

		txtTelefonoProveedor = new JTextField();
		txtTelefonoProveedor.setColumns(10);
		panel.add(txtTelefonoProveedor);

		lblEmpresaProveedor = new JLabel("Empresa");
		panel.add(lblEmpresaProveedor);

		txtEmpresaProveedor = new JTextField();
		panel.add(txtEmpresaProveedor);
		txtEmpresaProveedor.setColumns(10);

		btnAgregarNuevoProveedor = new JButton("Agregar");
		btnAgregarNuevoProveedor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// Guardamos el cliente
				if (idProveedor != 0) {

					modificarProveedor();
				} else {

					guardarProveedor();
				}
				dispose();
			}

		});
		panel.add(btnAgregarNuevoProveedor);

		btnCancelarNuevoProveedor = new JButton("Cancelar");
		btnCancelarNuevoProveedor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		panel.add(btnCancelarNuevoProveedor);

		// seccion para la edicion
		if (selectedValue != null) {

			String[] datosProveedor = selectedValue.split("-");
			this.idProveedor = Integer.valueOf(datosProveedor[0]);
			txtNombreProveedor.setText(datosProveedor[1]);
			txtApellidoProveedor.setText(datosProveedor[2]);
			txtDireccionProveedor.setText(datosProveedor[3]);
			txtTelefonoProveedor.setText(datosProveedor[4]);
			txtEmpresaProveedor.setText(datosProveedor[5]);
		}
	}

	private void guardarProveedor() {

		BaseDeDatos.agregarProveedor(new Proveedores(0, txtNombreProveedor.getText(), txtApellidoProveedor.getText(),
				txtDireccionProveedor.getText(), txtTelefonoProveedor.getText(), txtEmpresaProveedor.getText()));
	}

	private void modificarProveedor() {

		BaseDeDatos.actualizarProveedor(new Proveedores(this.idProveedor, txtNombreProveedor.getText(),
				txtApellidoProveedor.getText(), txtDireccionProveedor.getText(), txtTelefonoProveedor.getText(),
				txtEmpresaProveedor.getText()));
	}

}
