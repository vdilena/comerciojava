package interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AgregarArticulo extends JFrame {

	private JPanel contentPane;
	private JTextField txtCodigoArticulo;
	private JTextField txtArticuloArticulo;
	private JLabel lblCantidadAgregarArticulo;
	private JLabel lblCaracteristicaAgregarArticulo;
	private JTextField txtCaracteristicaArticulo;
	private JTextField txtCantidadArticulo;
	private JButton btnAgregarNuevoArticulo;
	private JButton btnCancelarNuevoArticulo;
	private int codigoArticulo = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AgregarArticulo frame = new AgregarArticulo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AgregarArticulo(String selectedValue) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 418);
		contentPane = new JPanel();
		contentPane.setBorder(new TitledBorder(null, "Agregar Art\u00EDculo", TitledBorder.LEADING, TitledBorder.TOP, null, Color.MAGENTA));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(5, 2, 5, 5));
		
		JLabel lblCodigoAgregarArticulo = new JLabel("C\u00F3digo");
		contentPane.add(lblCodigoAgregarArticulo);
		
		txtCodigoArticulo = new JTextField();
		//txtCodigoArticulo.setEditable(false);
		contentPane.add(txtCodigoArticulo);
		txtCodigoArticulo.setColumns(10);
		
		JLabel lblArticuloAgregarArticulo = new JLabel("Art\u00EDculo");
		contentPane.add(lblArticuloAgregarArticulo);
		
		txtArticuloArticulo = new JTextField();
		contentPane.add(txtArticuloArticulo);
		txtArticuloArticulo.setColumns(10);
		
		lblCantidadAgregarArticulo = new JLabel("Cantidad");
		contentPane.add(lblCantidadAgregarArticulo);
		
		txtCantidadArticulo = new JTextField();
		contentPane.add(txtCantidadArticulo);
		txtCantidadArticulo.setColumns(10);
		
		lblCaracteristicaAgregarArticulo = new JLabel("Caracteristica");
		contentPane.add(lblCaracteristicaAgregarArticulo);
		
		txtCaracteristicaArticulo = new JTextField();
		contentPane.add(txtCaracteristicaArticulo);
		txtCaracteristicaArticulo.setColumns(10);
		
		btnAgregarNuevoArticulo = new JButton("Agregar");
		btnAgregarNuevoArticulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// Guardamos el articulo
				if (codigoArticulo == 0) {

					guardarArticulo();
				} else {

					modificarArticulo();
				}
				dispose();
			}

		});
		contentPane.add(btnAgregarNuevoArticulo);
		
		btnCancelarNuevoArticulo = new JButton("Cancelar");
		btnCancelarNuevoArticulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		contentPane.add(btnCancelarNuevoArticulo);
		
		// seccion para la edicion
		if (selectedValue != null) {
			
			// Impido poder editar el codigo del articulo
			txtCodigoArticulo.setEditable(false);

			String[] datosArticulo = selectedValue.split("-");
			this.codigoArticulo = Integer.valueOf(datosArticulo[0]);
			txtCodigoArticulo.setText(datosArticulo[0]);
			txtArticuloArticulo.setText(datosArticulo[1]);
			txtCantidadArticulo.setText(datosArticulo[2]);
			txtCaracteristicaArticulo.setText(datosArticulo[3]);
		}
	}
	
	private void guardarArticulo() {
		
		try{
			
			BaseDeDatos.agregarArticulo(new Articulo(Integer.valueOf(txtCodigoArticulo.getText()).intValue(), txtArticuloArticulo.getText(),
					Integer.valueOf(txtCantidadArticulo.getText()).intValue(), txtCaracteristicaArticulo.getText()));
			
		} catch(NumberFormatException e){};

	}

	private void modificarArticulo() {

		BaseDeDatos.actualizarArticulo(new Articulo(Integer.valueOf(txtCodigoArticulo.getText()).intValue(), txtArticuloArticulo.getText(),
				Integer.valueOf(txtCantidadArticulo.getText()).intValue(), txtCaracteristicaArticulo.getText()));
	}

}
