package interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.Color;

import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.awt.event.ActionEvent;

public class Cliente extends JFrame {

	private JPanel contentPane;
	private JTextField txtBuscarCliente;
	List<Clientes> listadoClientes = new ArrayList<Clientes>();
	JList<String> jListClientes = null;
	JComboBox<String> cboBuscarCliente = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cliente frame = new Cliente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Cliente() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 819, 541);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Clientes", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(255, 0, 255)));
		contentPane.add(panel, BorderLayout.CENTER);

		// Listado de clientes
		cargarListadoClientes();
		this.jListClientes = new JList<String>(this.getListadoClientesFormateado());
		this.jListClientes.addListSelectionListener(listenerClientes());
		this.jListClientes.setBounds(12, 185, 757, 257);
		panel.add(this.jListClientes);

		JButton btnAgregarCliente = new JButton("Agregar ");
		btnAgregarCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AgregarCliente VerAgrCli = new AgregarCliente(null);
				VerAgrCli.setVisible(true);
				
				VerAgrCli.addWindowListener(actualizarListadoClientesAlAgregarListener());
			}
		});
		btnAgregarCliente.setBounds(12, 108, 127, 43);
		panel.add(btnAgregarCliente);

		JButton btnEliminarArticulo = new JButton("Eliminar ");
		btnEliminarArticulo.setBounds(172, 108, 127, 43);
		btnEliminarArticulo.addActionListener(listenerEliminar());
		panel.add(btnEliminarArticulo);

		JButton btnModificarCliente = new JButton("Modificar ");
		btnModificarCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				AgregarCliente verModCli = new AgregarCliente(jListClientes.getSelectedValue());
				verModCli.setVisible(true);
				
				verModCli.addWindowListener(actualizarListadoClientesAlAgregarListener());
			}
			
			
		});
		btnModificarCliente.setBounds(334, 108, 126, 43);
		panel.add(btnModificarCliente);

		txtBuscarCliente = new JTextField();
		txtBuscarCliente.setColumns(10);
		txtBuscarCliente.setBounds(506, 108, 116, 43);
		panel.add(txtBuscarCliente);

		JButton btnBuscarCliente = new JButton("Buscar");
		btnBuscarCliente.setBounds(648, 108, 121, 43);
		btnBuscarCliente.addActionListener(listenerBusqueda());
		panel.add(btnBuscarCliente);

		// Opciones de combo clientes
		cboBuscarCliente = new JComboBox(this.getOpcionesCliente());
		cboBuscarCliente.setBounds(650, 42, 119, 43);
		panel.add(cboBuscarCliente);

		JLabel lblBuscarCliente = new JLabel("Buscar por");
		lblBuscarCliente.setBounds(508, 42, 114, 43);
		panel.add(lblBuscarCliente);

		JButton btnSalirClientes = new JButton("Salir");
		btnSalirClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalirClientes.setBounds(630, 455, 127, 25);
		panel.add(btnSalirClientes);
	}

	/**
	 * 
	 * Nuevos metodos
	 */

	private void cargarListadoClientes() {

		this.listadoClientes = (List<Clientes>) BaseDeDatos.getListaClientesOProveedores(null, null, null, null, false);
	}

	private String [] getListadoClientesFormateado() {

		List<String> listaFormateada = new ArrayList<String>();

		for (Clientes cl : this.listadoClientes) {

			listaFormateada.add(cl.getId() 
					+ "-" + cl.getNombreC() 
					+ "-" + cl.getApellidoC() 
					+ "-" + cl.getDireccionC() 
					+ "-" + cl.getTelefonoC());
		}

		return listaFormateada.toArray(new String[0]);
	}

	private Vector<String> getOpcionesCliente() {

		Vector<String> comboOpciones = new Vector<String>();

		comboOpciones.addElement("Nombre");
		comboOpciones.addElement("Apellido");
		comboOpciones.addElement("Direccion");

		return comboOpciones;
	}

	private ListSelectionListener listenerClientes() {

		return new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					final String selectedValue = jListClientes.getSelectedValue();
				}
			}
		};
	}

	private ActionListener listenerBusqueda() {

		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String nombre = null;
				String apellido = null;
				String direccion = null;
				
				if (cboBuscarCliente.getSelectedItem().equals("Nombre")) {
					nombre = txtBuscarCliente.getText();
				}

				if (cboBuscarCliente.getSelectedItem().equals("Apellido")) {
					apellido = txtBuscarCliente.getText();
				}
				
				if (cboBuscarCliente.getSelectedItem().equals("Direccion")) {
					direccion = txtBuscarCliente.getText();
				}
				
				listadoClientes = (List<Clientes>) BaseDeDatos.getListaClientesOProveedores(nombre, apellido, direccion, null, false);
				jListClientes.setListData(getListadoClientesFormateado());
			}
		};
	}
	
	private ActionListener listenerEliminar() {
		
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					
					final String [] filaCliente = jListClientes.getSelectedValue().split("-");
					BaseDeDatos.eliminarClienteOProveedor(Integer.valueOf(filaCliente[0]));
					
					listadoClientes = (List<Clientes>) BaseDeDatos.getListaClientesOProveedores(null, null, null, null, false);
					jListClientes.setListData(getListadoClientesFormateado());
					
				} catch(NullPointerException excepcion) {
					
				}
			}
		};
	}
	
	private WindowAdapter actualizarListadoClientesAlAgregarListener(){
		
		return new WindowAdapter(){
            public void windowClosed(WindowEvent e){
            	listadoClientes = (List<Clientes>) BaseDeDatos.getListaClientesOProveedores(null, null, null, null, false);
				jListClientes.setListData(getListadoClientesFormateado());
            }
        };
	}

}
