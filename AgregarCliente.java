package interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AgregarCliente extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombreCliente;
	private JTextField txtApellidoCliente;
	private JTextField txtDireccionCliente;
	private JTextField txtTelefonoCliente;
	private int idCliente = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AgregarCliente frame = new AgregarCliente(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AgregarCliente(String selectedValue) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 623, 463);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Agregar Cliente",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 0, 255)));
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(5, 2, 5, 5));

		JLabel lblNombreAgregarCliente = new JLabel("Nombre");
		panel.add(lblNombreAgregarCliente);

		txtNombreCliente = new JTextField();
		txtNombreCliente.setColumns(10);
		panel.add(txtNombreCliente);

		JLabel lblApellidoAgregarCliente = new JLabel("Apellido");
		panel.add(lblApellidoAgregarCliente);

		txtApellidoCliente = new JTextField();
		txtApellidoCliente.setColumns(10);
		panel.add(txtApellidoCliente);

		JLabel lblDireccionAgregarCliente = new JLabel("Direcci\u00F3n");
		panel.add(lblDireccionAgregarCliente);

		txtDireccionCliente = new JTextField();
		txtDireccionCliente.setColumns(10);
		panel.add(txtDireccionCliente);

		JLabel lblTelefonoAgregarCliente = new JLabel("Tel\u00E9fono");
		panel.add(lblTelefonoAgregarCliente);

		txtTelefonoCliente = new JTextField();
		txtTelefonoCliente.setColumns(10);
		panel.add(txtTelefonoCliente);

		JButton btnAgregarNuevoCliente = new JButton("Agregar");
		btnAgregarNuevoCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// Guardamos el cliente
				if (idCliente != 0) {

					modificarCliente();
				} else {

					guardarCliente();
				}
				dispose();
			}

		});
		panel.add(btnAgregarNuevoCliente);

		JButton btnCancelarNuevoCliente = new JButton("Cancelar");
		btnCancelarNuevoCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		panel.add(btnCancelarNuevoCliente);

		// seccion para la edicion
		if (selectedValue != null) {

			String[] datosCliente = selectedValue.split("-");
			this.idCliente = Integer.valueOf(datosCliente[0]);
			txtNombreCliente.setText(datosCliente[1]);
			txtApellidoCliente.setText(datosCliente[2]);
			txtDireccionCliente.setText(datosCliente[3]);
			txtTelefonoCliente.setText(datosCliente[4]);
		}
	}

	private void guardarCliente() {

		BaseDeDatos.agregarCliente(new Clientes(0, txtNombreCliente.getText(), txtApellidoCliente.getText(),
				txtDireccionCliente.getText(), txtTelefonoCliente.getText()));
	}

	private void modificarCliente() {

		BaseDeDatos.actualizarCliente(new Clientes(this.idCliente, txtNombreCliente.getText(),
				txtApellidoCliente.getText(), txtDireccionCliente.getText(), txtTelefonoCliente.getText()));
	}

}
