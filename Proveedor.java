package interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.awt.event.ActionEvent;

public class Proveedor extends JFrame {

	private JPanel contentPane;
	private JTextField txtBuscarProveedor;
	List<Proveedores> listadoProveedores = new ArrayList<Proveedores>();
	JList<String> jListProveedores = null;
	JComboBox<String> cboBuscarProveedor = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Proveedor frame = new Proveedor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Proveedor() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 818, 552);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Proveedores", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 0, 255)));
		contentPane.add(panel, BorderLayout.CENTER);
		
		// Listado de proveedores
		cargarListadoProveedores();
		jListProveedores = new JList<String>(this.getListadoProveedoresFormateado());
		jListProveedores.addListSelectionListener(listenerProveedores());
		jListProveedores.setBounds(12, 177, 757, 265);
		panel.add(jListProveedores);
		
		JButton btnAgregarProveedor = new JButton("Agregar ");
		btnAgregarProveedor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AgregarProveedor verAgrProv= new AgregarProveedor(null);
					verAgrProv.setVisible(true);
					
					verAgrProv.addWindowListener(actualizarListadoProveedoresAlAgregarListener());
			}
		});
		btnAgregarProveedor.setBounds(12, 108, 127, 43);
		panel.add(btnAgregarProveedor);
		
		JButton btnEliminarCliente = new JButton("Eliminar ");
		btnEliminarCliente.setBounds(179, 108, 127, 43);
		btnEliminarCliente.addActionListener(listenerEliminar());
		panel.add(btnEliminarCliente);
		
		JButton btnModificarProveedor = new JButton("Modificar ");
		btnModificarProveedor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				AgregarProveedor verModProv = new AgregarProveedor(jListProveedores.getSelectedValue());
				verModProv.setVisible(true);
				
				verModProv.addWindowListener(actualizarListadoProveedoresAlAgregarListener());
			}
		});
		btnModificarProveedor.setBounds(331, 108, 126, 43);
		panel.add(btnModificarProveedor);
		
		txtBuscarProveedor = new JTextField();
		txtBuscarProveedor.setColumns(10);
		txtBuscarProveedor.setBounds(506, 108, 116, 43);
		panel.add(txtBuscarProveedor);
		
		JButton btnBuscarProveedor = new JButton("Buscar");
		btnBuscarProveedor.setBounds(648, 108, 121, 43);
		btnBuscarProveedor.addActionListener(listenerBusqueda());
		panel.add(btnBuscarProveedor);
		
		cboBuscarProveedor = new JComboBox(this.getOpcionesProveedor());
		cboBuscarProveedor.setBounds(648, 42, 119, 43);
		panel.add(cboBuscarProveedor);
		
		JLabel lblBuscarProveedor = new JLabel("Buscar por");
		lblBuscarProveedor.setBounds(508, 42, 114, 43);
		panel.add(lblBuscarProveedor);
		
		JButton btnSalirProveedores = new JButton("Salir");
		btnSalirProveedores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalirProveedores.setBounds(648, 457, 121, 25);
		panel.add(btnSalirProveedores);
	}
	
	/**
	 * 
	 * Nuevos metodos
	 */

	private void cargarListadoProveedores() {

		this.listadoProveedores = (List<Proveedores>) BaseDeDatos.getListaClientesOProveedores(null, null, null, null, true);
	}

	private String [] getListadoProveedoresFormateado() {

		List<String> listaFormateada = new ArrayList<String>();

		for (Proveedores pr : this.listadoProveedores) {

			listaFormateada.add(pr.getId() 
					+ "-" + pr.getNombreC() 
					+ "-" + pr.getApellidoC() 
					+ "-" + pr.getDireccionC() 
					+ "-" + pr.getTelefonoC()
					+ "-" + pr.getEmpresaP());
		}

		return listaFormateada.toArray(new String[0]);
	}
	
	private ListSelectionListener listenerProveedores() {

		return new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					final String selectedValue = jListProveedores.getSelectedValue();
				}
			}
		};
	}
	
	private WindowAdapter actualizarListadoProveedoresAlAgregarListener(){
		
		return new WindowAdapter(){
            public void windowClosed(WindowEvent e){
            	listadoProveedores = (List<Proveedores>) BaseDeDatos.getListaClientesOProveedores(null, null, null, null, true);
				jListProveedores.setListData(getListadoProveedoresFormateado());
            }
        };
	}
	
	private ActionListener listenerEliminar() {
		
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try{
				
					final String [] filaCliente = jListProveedores.getSelectedValue().split("-");
					BaseDeDatos.eliminarClienteOProveedor(Integer.valueOf(filaCliente[0]));
					
					listadoProveedores = (List<Proveedores>) BaseDeDatos.getListaClientesOProveedores(null, null, null, null, true);
					jListProveedores.setListData(getListadoProveedoresFormateado());
				
				} catch(NullPointerException excepcion) {
					
				}
			}
		};
	}
	
	private ActionListener listenerBusqueda() {

		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String nombre = null;
				String apellido = null;
				String direccion = null;
				String empresa = null;
				
				if (cboBuscarProveedor.getSelectedItem().equals("Nombre")) {
					nombre = txtBuscarProveedor.getText();
				}

				if (cboBuscarProveedor.getSelectedItem().equals("Apellido")) {
					apellido = txtBuscarProveedor.getText();
				}
				
				if (cboBuscarProveedor.getSelectedItem().equals("Direccion")) {
					direccion = txtBuscarProveedor.getText();
				}
				
				if (cboBuscarProveedor.getSelectedItem().equals("Empresa")) {
					empresa = txtBuscarProveedor.getText();
				}
				
				listadoProveedores = (List<Proveedores>) BaseDeDatos.getListaClientesOProveedores(nombre, apellido, direccion, empresa, true);
				jListProveedores.setListData(getListadoProveedoresFormateado());
			}
		};
	}
	
	private Vector<String> getOpcionesProveedor() {

		Vector<String> comboOpciones = new Vector<String>();

		comboOpciones.addElement("Nombre");
		comboOpciones.addElement("Apellido");
		comboOpciones.addElement("Direccion");
		comboOpciones.addElement("Empresa");

		return comboOpciones;
	}

}
