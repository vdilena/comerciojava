package interfaz;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class BaseDeDatos {

	// JDBC driver name and database URL
	private static final String JDBC_DRIVER = "org.h2.Driver";
	private static final String DB_URL = "jdbc:h2:tcp://localhost/~/comercio";

	// Database credentials
	private static final String USER = "sa";
	private static final String PASS = "";

	private static Connection connection = null;
	private static Statement statement = null;

	private static void connect() {

		try {

			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DB_URL, USER, PASS);

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

	}

	private static ResultSet getResultadoEjecutarSentencia(String query, boolean esParaGuardar) {

		ResultSet resultado = null;

		// Abro conexion
		try {

			// Ejecuto sentencia
			statement = connection.createStatement();
			
			if(!esParaGuardar) {
				resultado = statement.executeQuery(query);
			} else {
				statement.executeUpdate(query);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		// Devuelvo resultado
		return resultado;

	}

	private static void disconnect() {

		try {
			// Cierro conexion
			statement.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static List<Articulo> getListaArticulos(int codigo, String nombre, String caracteristicas) {
		
		connect();

		List<Articulo> articulos = new ArrayList<Articulo>();
		try {
			String query = "SELECT * FROM articulo ";

			if (codigo != 0) {
				query += "WHERE codigo = " + codigo;
			}

			if (nombre != null) {
				query += "WHERE nombre like '" + nombre + "%'";
			}
			
			if (caracteristicas != null) {
				query += "WHERE caracteristicas like '" + caracteristicas + "%'";
			}

			ResultSet resultado = getResultadoEjecutarSentencia(query, false);

			while (resultado.next()) {
				Articulo itemArticulo = new Articulo(resultado.getInt("codigo"), resultado.getString("nombre"),
						resultado.getInt("cantidad"), resultado.getString("caracteristicas"));
				articulos.add(itemArticulo);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		disconnect();

		return articulos;

	}
	
	public static List<? extends Clientes> getListaClientesOProveedores(String nombre, String apellido, String direccion, String empresa, boolean esProveedor) {
		
		connect();
		
		List<Clientes> clientesARetornar = new ArrayList<Clientes>();
		List<Proveedores> proveedoresARetornar = new ArrayList<Proveedores>();
			
		try {
			String query = "SELECT * FROM cliente WHERE es_proveedor = " + esProveedor + " ";

			if (nombre != null) {
				query += "AND nombre like '" + nombre + "%' ";
			}

			if (apellido != null) {
				query += "AND apellido like '" + apellido + "%' ";
			}
			
			if (empresa != null) {
				query += "AND empresa like '" + empresa + "%' ";
			}
			
			if (direccion != null) {
				query += "AND direccion like '" + direccion + "%' ";
			}

			ResultSet resultado = getResultadoEjecutarSentencia(query, false);

			while (resultado.next()) {
				
				Clientes itemCliente = null;
				if(!esProveedor){
					
					itemCliente = new Clientes(
							resultado.getInt("id"),
							resultado.getString("nombre"), 
							resultado.getString("apellido"),
							resultado.getString("direccion"), 
							resultado.getString("telefono")
							);
				} else {
					itemCliente = new Proveedores(
							resultado.getInt("id"),
							resultado.getString("nombre"), 
							resultado.getString("apellido"),
							resultado.getString("direccion"), 
							resultado.getString("telefono"),
							resultado.getString("empresa")
							);
				}
				
				if(!esProveedor) {
					
					clientesARetornar.add(itemCliente);
				} else {
					
					proveedoresARetornar.add((Proveedores) itemCliente);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		disconnect();
		
		clientesARetornar.addAll(proveedoresARetornar);

		return clientesARetornar;

	}
	
	public static Articulo agregarArticulo(Articulo nuevoArticulo) {
		
		connect();

		String query = "INSERT INTO ARTICULO (codigo, nombre, cantidad, caracteristicas) VALUES ( " 
			+ nuevoArticulo.getCodigoA()
			+ ",'" + nuevoArticulo.getArticuloA() + "',"
			+ nuevoArticulo.getCantidadA()
			+ ",'" + nuevoArticulo.getCaracteristicaA() + "'"
			+ " )";
		getResultadoEjecutarSentencia(query, true);
		
		disconnect();
		
		return nuevoArticulo;
	}
	
	public static Articulo actualizarArticulo(Articulo articuloActualizado) {
		
		connect();
			
		String query = "UPDATE ARTICULO SET " 
			+ "NOMBRE = '" + articuloActualizado.getArticuloA() + "',"
			+ "CANTIDAD = " + articuloActualizado.getCantidadA() + ","
			+ "CARACTERISTICAS = '" + articuloActualizado.getCaracteristicaA() + "'"
			+ " WHERE CODIGO = " + articuloActualizado.getCodigoA();
		getResultadoEjecutarSentencia(query, true);
		
		disconnect();
		
		return articuloActualizado;
	}
	
	public static void eliminarArticulo(int codigoArticulo) {
		
		connect();
			
		String query = "DELETE FROM ARTICULO WHERE CODIGO = " + codigoArticulo;
		getResultadoEjecutarSentencia(query, true);
		
		disconnect();

	}
	
	public static Clientes agregarCliente(Clientes nuevoCliente) {
		
		connect();

		String query = "INSERT INTO CLIENTE (NOMBRE, APELLIDO, DIRECCION, TELEFONO, EMPRESA, ES_PROVEEDOR) VALUES ( " 
			+ "'" +  nuevoCliente.getNombreC() + "'," 
			+ "'" + nuevoCliente.getApellidoC() + "',"
			+ "'" + nuevoCliente.getDireccionC() + "',"
			+ "'" + nuevoCliente.getTelefonoC() + "',"
			+ "NULL, FALSE )";
		getResultadoEjecutarSentencia(query, true);
		
		disconnect();
		
		return nuevoCliente;
	}
	
	public static Proveedores agregarProveedor(Proveedores nuevoProveedor) {
		
		connect();

		String query = "INSERT INTO CLIENTE (NOMBRE, APELLIDO, DIRECCION, TELEFONO, EMPRESA, ES_PROVEEDOR) VALUES ( " 
			+ "'" +  nuevoProveedor.getNombreC() + "'," 
			+ "'" + nuevoProveedor.getApellidoC() + "',"
			+ "'" + nuevoProveedor.getDireccionC() + "',"
			+ "'" + nuevoProveedor.getTelefonoC() + "',"
			+ "'" + nuevoProveedor.getEmpresaP() + "',"
			+ "TRUE )";
		getResultadoEjecutarSentencia(query, true);
		
		disconnect();
		
		return nuevoProveedor;
	}
	
	public static Clientes actualizarCliente(Clientes clienteActualizado) {
		
		connect();
			
		String query = "UPDATE CLIENTE SET " 
			+ "NOMBRE = '" + clienteActualizado.getNombreC() + "',"
			+ "APELLIDO = '" + clienteActualizado.getApellidoC() + "',"
			+ "DIRECCION = '" + clienteActualizado.getDireccionC() + "',"
			+ "TELEFONO = '" + clienteActualizado.getTelefonoC() + "'"
			+ " WHERE ID = " + clienteActualizado.getId();
		getResultadoEjecutarSentencia(query, true);
		
		disconnect();
		
		return clienteActualizado;
	}
	
	public static Proveedores actualizarProveedor(Proveedores proveedor) {
		
		connect();
			
		String query = "UPDATE CLIENTE SET " 
			+ "NOMBRE = '" + proveedor.getNombreC() + "',"
			+ "APELLIDO = '" + proveedor.getApellidoC() + "',"
			+ "DIRECCION = '" + proveedor.getDireccionC() + "',"
			+ "TELEFONO = '" + proveedor.getTelefonoC() + "',"
			+ "EMPRESA = '" + proveedor.getEmpresaP() + "'"
			+ " WHERE ID = " + proveedor.getId();
		getResultadoEjecutarSentencia(query, true);
		
		disconnect();
		
		return proveedor;
	}
	
	public static void eliminarClienteOProveedor(int idClienteOProveedor) {
		
		connect();
			
		String query = "DELETE FROM CLIENTE WHERE ID = " + idClienteOProveedor;
		getResultadoEjecutarSentencia(query, true);
		
		disconnect();

	}

}
