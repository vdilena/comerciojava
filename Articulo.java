package interfaz;

public class Articulo {
	private int codigoA;
	private String articuloA;
	private int cantidadA;
	private String caracteristicaA;
	
	
	
	public Articulo(int codigoA, String articuloA, int cantidadA,
			String caracteristicaA) {
		super();
		this.codigoA = codigoA;
		this.articuloA = articuloA;
		this.cantidadA = cantidadA;
		this.caracteristicaA = caracteristicaA;
	}
	public int getCodigoA() {
		return codigoA;
	}
	public void setCodigoA(int codigoA) {
		this.codigoA = codigoA;
	}
	public String getArticuloA() {
		return articuloA;
	}
	public void setArticuloA(String articuloA) {
		this.articuloA = articuloA;
	}
	public int getCantidadA() {
		return cantidadA;
	}
	public void setCantidadA(int cantidadA) {
		this.cantidadA = cantidadA;
	}
	public String getCaracteristicaA() {
		return caracteristicaA;
	}
	public void setCaracteristicaA(String caracteristicaA) {
		this.caracteristicaA = caracteristicaA;
	}
	
	

}
