package interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.Color;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.awt.event.ActionEvent;

public class Articulos extends JFrame {

	private JPanel contentPane;
	private JButton btnEliminarArticulo;
	private JTextField txtBuscarArticulo;
	List<Articulo> listadoArticulos = new ArrayList<Articulo>();
	JList<String> jListArticulos = null;
	JComboBox<String> cboBuscarArticulo = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Articulos frame = new Articulos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Articulos() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 799, 544);
		contentPane = new JPanel();
		contentPane.setBorder(new TitledBorder(null, "Art\u00EDculos", TitledBorder.LEADING, TitledBorder.TOP, null, Color.MAGENTA));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Listado de articulos
		cargarListadoArticulo();
		jListArticulos = new JList<String>(this.getListadoArticuloFormateado());
		jListArticulos.addListSelectionListener(listenerArticulo());
		jListArticulos.setBounds(12, 185, 757, 257);
		contentPane.add(jListArticulos);
		
		JButton btnAgregarArticulo = new JButton("Agregar ");
		btnAgregarArticulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AgregarArticulo verAgrArt= new AgregarArticulo(null);
					verAgrArt.setVisible(true);
					
					verAgrArt.addWindowListener(actualizarListadoArticuloAlAgregarListener());
			}
		});
		btnAgregarArticulo.setBounds(12, 108, 127, 43);
		contentPane.add(btnAgregarArticulo);
		
		btnEliminarArticulo = new JButton("Eliminar ");
		btnEliminarArticulo.setBounds(172, 108, 127, 43);
		btnEliminarArticulo.addActionListener(listenerEliminar());
		contentPane.add(btnEliminarArticulo);
		
		JButton btnModificarArticulo = new JButton("Modificar ");
		btnModificarArticulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*AgregarArticulo VerModArt = new AgregarArticulo();
					VerModArt.setVisible(true);*/
				
				AgregarArticulo VerModArt = new AgregarArticulo(jListArticulos.getSelectedValue());
				VerModArt.setVisible(true);
				
				VerModArt.addWindowListener(actualizarListadoArticuloAlAgregarListener());
			}
		});
		btnModificarArticulo.setBounds(334, 108, 126, 43);
		contentPane.add(btnModificarArticulo);
		
		txtBuscarArticulo = new JTextField();
		txtBuscarArticulo.setBounds(506, 108, 116, 43);
		contentPane.add(txtBuscarArticulo);
		txtBuscarArticulo.setColumns(10);
		
		JButton btnBuscarArticulo = new JButton("Buscar");
		btnBuscarArticulo.setBounds(648, 108, 121, 43);
		btnBuscarArticulo.addActionListener(listenerBusqueda());
		contentPane.add(btnBuscarArticulo);
		
		cboBuscarArticulo = new JComboBox(this.getOpcionesArticulos());
		cboBuscarArticulo.setBounds(650, 42, 119, 43);
		contentPane.add(cboBuscarArticulo);
		
		JLabel lblBuscarArticulo = new JLabel("Buscar por");
		lblBuscarArticulo.setBounds(508, 42, 114, 43);
		contentPane.add(lblBuscarArticulo);
		
		JButton btnSalirArticulos = new JButton("Salir");
		btnSalirArticulos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalirArticulos.setBounds(634, 455, 116, 29);
		contentPane.add(btnSalirArticulos);
	}
	
	/**
	 * 
	 * Nuevos metodos
	 */

	private void cargarListadoArticulo() {

		this.listadoArticulos = (List<Articulo>) BaseDeDatos.getListaArticulos(0, null, null);
	}

	private String [] getListadoArticuloFormateado() {

		List<String> listaFormateada = new ArrayList<String>();

		for (Articulo art : this.listadoArticulos) {

			listaFormateada.add(art.getCodigoA() 
					+ "-" + art.getArticuloA() 
					+ "-" + art.getCantidadA() 
					+ "-" + art.getCaracteristicaA());
		}

		return listaFormateada.toArray(new String[0]);
	}
	
	private ListSelectionListener listenerArticulo() {

		return new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					final String selectedValue = jListArticulos.getSelectedValue();
				}
			}
		};
	}
	
	private WindowAdapter actualizarListadoArticuloAlAgregarListener(){
		
		return new WindowAdapter(){
            public void windowClosed(WindowEvent e){
            	listadoArticulos = (List<Articulo>) BaseDeDatos.getListaArticulos(0, null, null);
				jListArticulos.setListData(getListadoArticuloFormateado());
            }
        };
	}
	
	private ActionListener listenerEliminar() {
		
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try{
				
					final String [] filaArticulo = jListArticulos.getSelectedValue().split("-");
					BaseDeDatos.eliminarArticulo(Integer.valueOf(filaArticulo[0]));
					
					listadoArticulos = (List<Articulo>) BaseDeDatos.getListaArticulos(0, null, null);
					jListArticulos.setListData(getListadoArticuloFormateado());
				
				} catch(NullPointerException excepcion) {
					
				}
			}
		};
	}
	
	private ActionListener listenerBusqueda() {

		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int codigo = 0;
				String nombre = null;
				String caracteristicas = null;
				
				try {
				
					if (cboBuscarArticulo.getSelectedItem().equals("Codigo") && !txtBuscarArticulo.getText().equals("")) {
						codigo = Integer.valueOf(txtBuscarArticulo.getText()).intValue();
					} 
	
					if (cboBuscarArticulo.getSelectedItem().equals("Nombre")) {
						nombre = txtBuscarArticulo.getText();
					}
					
					if (cboBuscarArticulo.getSelectedItem().equals("Caracteristicas")) {
						caracteristicas = txtBuscarArticulo.getText();
					}
					
					listadoArticulos = (List<Articulo>) BaseDeDatos.getListaArticulos(codigo, nombre, caracteristicas);
					jListArticulos.setListData(getListadoArticuloFormateado());
					
				} catch(NumberFormatException errorFormato){
					String [] listaVacia = {};
					jListArticulos.setListData(listaVacia);
				}
			}
		};
	}
	
	private Vector<String> getOpcionesArticulos() {

		Vector<String> comboOpciones = new Vector<String>();

		comboOpciones.addElement("Codigo");
		comboOpciones.addElement("Nombre");
		comboOpciones.addElement("Caracteristicas");

		return comboOpciones;
	}
}
