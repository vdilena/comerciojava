package interfaz;

public class Clientes {

	private int id;
	private String nombreC;
	private String apellidoC;
	private String direccionC;
	private String telefonoC;

	public Clientes() {
		nombreC = " ";
		apellidoC = " ";
		direccionC = " ";
		telefonoC = "";
	}

	public Clientes(int id, String nombreC, String apellidoC, String direccionC, String telefonoC) {
		this.id = id;
		this.nombreC = nombreC;
		this.apellidoC = apellidoC;
		this.direccionC = direccionC;
		this.telefonoC = telefonoC;
	}

	public String getNombreC() {
		return nombreC;
	}

	public void setNombreC(String nombreC) {
		this.nombreC = nombreC;
	}

	public String getApellidoC() {
		return apellidoC;
	}

	public void setApellidoC(String apellidoC) {
		this.apellidoC = apellidoC;
	}

	public String getDireccionC() {
		return direccionC;
	}

	public void setDireccionC(String direccionC) {
		this.direccionC = direccionC;
	}

	public String getTelefonoC() {
		return telefonoC;
	}

	public void setTelefonoC(String telefonoC) {
		this.telefonoC = telefonoC;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}

class Proveedores extends Clientes {

	private String empresaP;

	public Proveedores(int id, String nombreC, String apellidoC, String direccionC, String telefonoC, String empresa) {
		super(id, nombreC, apellidoC, direccionC, telefonoC);
		this.empresaP = empresa;

	}

	public String getEmpresaP() {
		return empresaP;
	}

	public void setEmpresaP(String empresaP) {
		this.empresaP = empresaP;
	}
}